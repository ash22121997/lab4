
  #include "stm32f1xx.h"
#include <string.h>

typedef struct {
	uint8_t *buf;     //указатель на место хранения буфера
	uint32_t head;    //индекс начала
	uint32_t tail;    //индекс конца
	uint32_t length;  //количество символов в буффере
	uint32_t size;    //размер буфера
	uint8_t full;     //флаг переполнения
	uint8_t empty;    //флаг пустоты
} RingBuffer;

UART_HandleTypeDef huartPC;
UART_HandleTypeDef huartSTM;
RingBuffer uartPC;
RingBuffer uartSTM;

void System_ClockInit(void);
void huartPC_init(void);
void huartSTM_init(void);
void circle_init(RingBuffer *uartname, uint8_t *BufferUart, uint32_t size);
uint8_t Ring_push(RingBuffer *uartname, void *data);
uint8_t Ring_pop(RingBuffer *uartname, void *data);
uint8_t Ring_empty(RingBuffer *uartname);
uint8_t Ring_full(RingBuffer *uartname);

uint8_t ReciveCharPC;
uint8_t TransmitCharPC;
uint8_t ReciveCharSTM;
uint8_t TransmitCharSTM;

uint8_t FlagTXuartPC = 1;
uint8_t FlagTXuartSTM = 1;

char BufferUartPC[256];
char BufferUartSTM[256];

int main(void){
	HAL_Init();
	System_ClockInit();
	huartPC_init();
	huartSTM_init();

	circle_init(&uartPC, &BufferUartPC[0],  sizeof(BufferUartPC)-1);
	circle_init(&uartSTM, &BufferUartSTM[0],  sizeof(BufferUartSTM)-1);
	HAL_UART_Receive_IT(&huartPC, (uint8_t*)&ReciveCharPC, 1);
	HAL_UART_Receive_IT(&huartSTM, (uint8_t*)&ReciveCharSTM, 1);

	for(;;){
		if ((!Ring_empty(&uartPC))&&(FlagTXuartSTM)){
			Ring_pop(&uartPC, &TransmitCharPC);
		  FlagTXuartSTM = 0;
		  HAL_UART_Transmit_IT(&huartSTM, &TransmitCharPC, 1);
		}
		if ((!Ring_empty(&uartSTM))&&(FlagTXuartPC)){
			Ring_pop(&uartSTM, &TransmitCharSTM);
		  FlagTXuartPC = 0;
		  HAL_UART_Transmit_IT(&huartPC, &TransmitCharSTM, 1);
		}
	}
}
//------------------------------------------------------------------------------------------------------
void HAL_UART_MspInit(UART_HandleTypeDef *huart){
	if(huart == &huartPC){
		__HAL_RCC_USART1_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		GPIO_InitTypeDef gpioInit;

		gpioInit.Mode = GPIO_MODE_AF_INPUT;
		gpioInit.Pin = GPIO_PIN_10; //RX
		gpioInit.Pull = GPIO_NOPULL;
		gpioInit.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOA, &gpioInit);

		gpioInit.Mode = GPIO_MODE_AF_PP;
		gpioInit.Pin = GPIO_PIN_9; //TX
		gpioInit.Pull = GPIO_NOPULL;
		gpioInit.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOA, &gpioInit);
	}
	if(huart == &huartSTM){
		__HAL_RCC_USART2_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		GPIO_InitTypeDef gpioInit;

		gpioInit.Mode = GPIO_MODE_AF_INPUT;
		gpioInit.Pin = GPIO_PIN_3; //RX
		gpioInit.Pull = GPIO_NOPULL;
		gpioInit.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOA, &gpioInit);

		gpioInit.Mode = GPIO_MODE_AF_PP;
		gpioInit.Pin = GPIO_PIN_2; //TX
		gpioInit.Pull = GPIO_NOPULL;
		gpioInit.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOA, &gpioInit);
	}
}
//-------------------------------------------------------------------------------------------------------
void System_ClockInit(){
	RCC_OscInitTypeDef osc;
	RCC_ClkInitTypeDef clk;

	osc.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	osc.HSEState = RCC_HSE_ON;
	osc.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	osc.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	osc.PLL.PLLMUL = RCC_PLL_MUL9;
	osc.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	osc.PLL.PLLState = RCC_PLL_ON;
	if (HAL_RCC_OscConfig(&osc) != HAL_OK){
		while(1);
	}
	clk.AHBCLKDivider = RCC_SYSCLK_DIV1;
	clk.APB1CLKDivider = RCC_HCLK_DIV2;
	clk.APB2CLKDivider = RCC_HCLK_DIV1;
	clk.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	clk.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	if (HAL_RCC_ClockConfig(&clk, FLASH_LATENCY_2) != HAL_OK){
		while(1);
	}
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
//-------------------------------------------------------------------------------------------------------
void huartPC_init(void){
	huartPC.Instance = USART1;
	huartPC.Init.BaudRate = 9600;  //9600 бод
	huartPC.Init.WordLength = UART_WORDLENGTH_9B;//9B
	huartPC.Init.StopBits = UART_STOPBITS_1;
	huartPC.Init.Parity = UART_PARITY_EVEN; //EVEN проверка четности на четность
	huartPC.Init.Mode = UART_MODE_TX_RX;
	huartPC.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huartPC.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huartPC) != HAL_OK){
		while(1);
	}
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
}
//-------------------------------------------------------------------------------------------------------
void huartSTM_init(void){
	huartSTM.Instance = USART2;
	huartSTM.Init.BaudRate = 115200;
	huartSTM.Init.WordLength = UART_WORDLENGTH_9B;
	huartSTM.Init.StopBits = UART_STOPBITS_2;
	huartSTM.Init.Parity = UART_PARITY_EVEN;
	huartSTM.Init.Mode = UART_MODE_TX_RX;
	huartSTM.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huartSTM.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huartSTM) != HAL_OK){
		while(1);
	}
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
}
//-------------------------------------------------------------------------------------------------------
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart == &huartPC){
		HAL_UART_Receive_IT(&huartPC, &ReciveCharPC, 1);
		Ring_push(&uartPC,&ReciveCharPC);
	}
	if(huart == &huartSTM){
			HAL_UART_Receive_IT(&huartSTM, &ReciveCharSTM, 1);
			Ring_push(&uartSTM,&ReciveCharSTM);
	}
}
//-------------------------------------------------------------------------------------------------------
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart == &huartPC){
		FlagTXuartPC = 1;
	}
	if(huart == &huartSTM){
		FlagTXuartSTM = 1;
	}
}
//----------------------------------------Ring--buffer---------------------------------------------------
void circle_init(RingBuffer *uartname, uint8_t *BufferUart,  uint32_t size){
	uartname->buf = BufferUart;
	uartname->size = size;
	uartname->empty = 1;
}
//-------------------------------------------------------------------------------------------------------
uint8_t Ring_push(RingBuffer *uartname, void *data){
	__disable_irq();
	if (uartname->full) {
		__enable_irq();
		return 0;
	}
	memcpy(&uartname->buf[uartname->head], data, 1);
	uartname->length++;
	uartname->head += 1;

	if (uartname->head == uartname->size) {
		uartname->head = 0;
	}
	if (uartname->head == uartname->tail) {
		uartname->full = 1;
	}
	else
		uartname->empty = 0;

	__enable_irq();
	return 1;
}
//-------------------------------------------------------------------------------------------------------
uint8_t Ring_pop(RingBuffer *uartname, void *data){
	__disable_irq();
	if (uartname->empty) {
		__enable_irq();
		return 0;
	}
	memcpy(data, &uartname->buf[uartname->tail], 1);
	uartname->length--;
	uartname->tail += 1;

	if (uartname->tail == uartname->size) {
		uartname->tail = 0;
	}

	if (uartname->head == uartname->tail) {
		uartname->empty = 1;
	}
	else
		uartname->full = 0;

	__enable_irq();
	return 1;
}
//-------------------------------------------------------------------------------------------------------
uint8_t Ring_empty(RingBuffer *uartname){
	__disable_irq();
	uint8_t empty = uartname->empty;
	__enable_irq();
	return empty;
}

uint8_t Ring_full(RingBuffer *uartname){
	__disable_irq();
	uint8_t full = uartname->full;
	__enable_irq();
	return full;
}
